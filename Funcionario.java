public class Funcionario {

private String nome;
private double salario;
private String rg;
private String departamento;
private String dataEntrada;


public void setNome(String umNome){
nome = umNome; 
}

public void setSalario(double umSalario){
salario = umSalario;
}

public void setRg(String umRg){
rg=umRg;
}

public void setDepartamento(String umDepartamento){
departamento = umDepartamento;
}

public void setDataEntrada(String umaDataEntrada){
dataEntrada = umaDataEntrada;
}



public String getNome(){
return nome;
}

public double getSalario(){
return salario;
}

public String getRg(){
return rg;
}

public String getDepartamento(){
return departamento;
}

public String getDataEntrada(){
return dataEntrada;
}



public void recebeAumento(double aumento) {

salario = salario + aumento;
}
public double calculaGanhoAnual() {
double ganhoAnual = salario * 12;
return ganhoAnual;
}
}


