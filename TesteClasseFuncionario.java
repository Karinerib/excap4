import java.util.*;

public class TesteClasseFuncionario
{

public static void main(String[] args) {
Scanner lerTeclado = new Scanner(System.in);
Funcionario umFuncionario = new Funcionario();
ArrayList<Funcionario> listaFuncionarios = new ArrayList<Funcionario>();
	
System.out.println(" Digite o salário do Funcionário: ");
double umSalario = Double.parseDouble(lerTeclado.nextLine());
umFuncionario.setSalario(umSalario);

System.out.println(" Digite o nome do Funcionário: ");
String umNome = lerTeclado.nextLine();
umFuncionario.setNome(umNome);
	
System.out.println("Nome: " + umFuncionario.getNome());
System.out.println("Salario: " + umFuncionario.getSalario());	
}
}
